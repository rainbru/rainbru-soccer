/*
 * Copyright (C) 2016-2019 Jérôme Pasquier
 * Copyright (C) 2003-2011 by Tux Football development team
 * Authors: Jason Wood <jasonwood@blueyonder.co.uk>
 *          Christoph Brill <egore911@egore911.de>
 *
 * This file is part of rainbru-soccer.
 *
 * rainbru-soccer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbru-soccer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbru-soccer.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef TUXFOOTBALL_TILEMAP
#define TUXFOOTBALL_TILEMAP

#include <string>
#include <vector>
#include "SDL.h"

/** Draws "tiles". A tile map specifies a number of bitmap images, and then a
 * grid specifying where each of those images should be placed. This allows for
 * efficient management of large images with small amounts of unique detail -
 * for instance, with pitch lines. */
class TileMap {
public:
  TileMap(SDL_Window *screen, SDL_Renderer *renderer, std::string path);
  ~TileMap();

  void draw(int left, int top);	
private:
  int m_tileWidth;   //!< The width of a tile (i.e. fixed to 16)
  int m_tileHeight;  //!< The height of a tile (i.e. fixed to 16)
  int m_tileMapWidth; //!< The width of the tilemap (i.e. 2560)
  int m_tileMapHeight;//!< The height of the tilemap (i.e. 4096)
  int m_numTilesWidth; //!< The number of horizontal tiles to fill tilemap
  int m_numTilesHeight;//!< The number of vertical tiles to fill tilemap
  std::vector<SDL_Texture *> m_tileSurfaces;
  SDL_Window *m_screen;     //!< The current SDL window
  SDL_Renderer *m_renderer; //!< The current SDL renderer
  
  SDL_Texture *tileSurface(int x, int y);	
  void setTileSurface(int x, int y, SDL_Texture *surface);
  bool isWhiteSpace(char val);
};

#endif // TUXFOOTBALL_TILEMAP
