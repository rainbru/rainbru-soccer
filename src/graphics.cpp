/*
 * Copyright (C) 2016-2019 Jérôme Pasquier
 * Copyright (C) 2003-2011 by Tux Football development team
 * Authors: Jason Wood <jasonwood@blueyonder.co.uk>
 *          Christoph Brill <egore911@egore911.de>
 *
 * This file is part of rainbru-soccer.
 *
 * rainbru-soccer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbru-soccer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbru-soccer.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "graphics.h"

#include <iostream>

#include "logger/logger.h"

Graphics::Graphics(SDL_Window *screen, SDL_Renderer* renderer)
{
	m_screen = screen;
	m_renderer = renderer;
}

Graphics::~Graphics()
{
}

void Graphics::addSprite(SpriteObject *obj)
{
	m_sprites.push_back(SpriteContainer(obj));
}

void Graphics::removeSprite(SpriteObject *obj)
{
	std::list<SpriteContainer>::iterator itt;

	for(itt = m_sprites.begin(); itt!=m_sprites.end(); ++itt) {
		if((*(*itt))==obj) {
			m_sprites.erase(itt);
			return;
		}
	}
	WARN("Graphics::removeSprite() : sprite does not exist");
}

void Graphics::draw(int left, int top)
{
	std::list<SpriteContainer>::iterator itt;

	for(itt=m_sprites.begin(); itt!=m_sprites.end(); ++itt) {
		(*(*itt))->drawShadow(left, top, m_renderer);
	}
	for(itt=m_sprites.begin(); itt!=m_sprites.end(); ++itt) {
		(*(*itt))->draw(left, top, m_renderer);
	}
	for(itt=m_sprites.begin(); itt!=m_sprites.end(); ++itt) {
		(*(*itt))->drawOverlay(left, top, m_renderer);
	}
}

void Graphics::update()
{
	m_sprites.sort();
}

SDL_Window *Graphics::screen() {
	return m_screen;
}

SDL_Renderer *Graphics::renderer() {
	return m_renderer;
}
