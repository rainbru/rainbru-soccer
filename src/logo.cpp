/*
 * Copyright (C) 2016-2019 Jérôme Pasquier
 * Copyright (C) 2003-2011 by Tux Football development team
 * Authors: Jason Wood <jasonwood@blueyonder.co.uk>
 *          Christoph Brill <egore911@egore911.de>
 *
 * This file is part of rainbru-soccer.
 *
 * rainbru-soccer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbru-soccer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbru-soccer.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "logo.h"

#include "config.h" // Uses TITLE

#include "gameengine.h"
#include "resources/surfacemanager.h"
#include "resources/fontmanager.h"

#include "logger/logger.h"



/** The default constructor
  *
  * If legacy is true, draw the legacy graphic-based logo, otherwise,
  * print the new one.
  *
  */
Logo::Logo(GameEngine& ge):
  m_logo(NULL),
  m_engine(ge)
{
  INFO("Loading logo");
  m_logo = SurfaceManager::instance(m_engine.renderer())
    ->load(LOGO_FILENAME);
}

Logo::~Logo()
{
  if(m_logo) {
    SurfaceManager::instance(m_engine.renderer())->release(m_logo);
    m_logo = NULL;
  }
  
}

void
Logo::draw()
{
  if (m_logo)
    {
      SDL_Rect srcrect, dstrect;
      int renderer_width, renderer_height, logo_width, logo_height;
      SDL_GetRendererOutputSize(m_engine.renderer(), &renderer_width,
				&renderer_height);
      SDL_QueryTexture(m_logo, NULL, NULL, &logo_width, &logo_height);
      
      dstrect.x = (renderer_width - logo_width) / 2;
      dstrect.y = 50;
      srcrect.x = 0;
      srcrect.y = 0;
      dstrect.w = srcrect.w = logo_width;
      dstrect.h = srcrect.h = logo_height;
      
      if(SDL_RenderCopy(m_engine.renderer(), m_logo,
			&srcrect, &dstrect)<0){
	
	ERROR("could not pitch tile : " << SDL_GetError());
      }
    }

}
