/*
 * Copyright (C) 2016-2019 Jérôme Pasquier
 * Copyright (C) 2003-2011 by Tux Football development team
 * Authors: Jason Wood <jasonwood@blueyonder.co.uk>
 *          Christoph Brill <egore911@egore911.de>
 *
 * This file is part of rainbru-soccer.
 *
 * rainbru-soccer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbru-soccer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbru-soccer.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LOGO_H__
#define __LOGO_H__

#include <SDL.h>

// The data/ filename of the legacy logo
#define LOGO_FILENAME "graphics/rainbru-soccer.png"


// Forward declarations
class GameEngine;
// End of forward declarations


/** A logo class designed to switch between legacy tuxfootball and
  * new, font-based rainbru-socer one
  *
  */
class Logo
{
 public:
  Logo(GameEngine&);
  ~Logo();

  void draw();
  
 private:
  SDL_Texture* m_logo;        //!< The texture used to draw legacy logo
  GameEngine& m_engine;       //!< The engine used to render/release textures
  
};

#endif // !__LOGO_H__
