/*
 * Copyright (C) 2016-2019 Jérôme Pasquier
 * Copyright (C) 2003-2011 by Tux Football development team
 * Authors: Jason Wood <jasonwood@blueyonder.co.uk>
 *          Christoph Brill <egore911@egore911.de>
 *
 * This file is part of rainbru-soccer.
 *
 * rainbru-soccer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbru-soccer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbru-soccer.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef TUXFOOTBALL_FIRSTHALFSTATE
#define TUXFOOTBALL_FIRSTHALFSTATE

#include "ingamestatebase.h"

class GameEngine;

class FirstHalfState : public IngameStateBase {
public:
	FirstHalfState(GameEngine &engine);
	virtual ~FirstHalfState();

	/** Called when the state is entered. Occurs before the update loop method is called. */
	void enterState();

	/** Called when leaving the state, after the last updateLoop has finished. */
	void leaveState();

	/** The main logic for the state should be in updateLoop. */
	void updateLoop();

};
#endif /* TUXFOOTBALL_FIRSTHALFSTATE */
