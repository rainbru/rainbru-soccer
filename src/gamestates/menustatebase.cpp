/*
 * Copyright (C) 2016-2019 Jérôme Pasquier
 * Copyright (C) 2003-2011 by Tux Football development team
 * Authors: Jason Wood <jasonwood@blueyonder.co.uk>
 *          Christoph Brill <egore911@egore911.de>
 *
 * This file is part of rainbru-soccer.
 *
 * rainbru-soccer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbru-soccer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbru-soccer.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "menustatebase.h"

#include <iostream>
#include <SDL.h>

#include "gameengine.h"
#include "resources/surfacemanager.h"
#include "menu/menu.h"
#include "logger/logger.h"

#include "logo.h"

MenuStateBase::MenuStateBase(GameEngine &engine):
  m_engine(engine), m_logo(NULL), m_menu(0)
{
}

MenuStateBase::~MenuStateBase()
{
  delete m_logo;
  m_logo = NULL;
    
  setMenu(0);
}

bool MenuStateBase::isGameInProgress() const
{
  return false;
}

void MenuStateBase::renderFrame()
{
  // Draw Menu
  if(m_menu)
    m_menu->draw();

  if(m_logo)
    m_logo->draw();
}

void MenuStateBase::enterState()
{
  initialiseMenu();
  m_logo = new Logo(m_engine);
}

void MenuStateBase::setMenu(Menu *menu)
{
	if(m_menu) {
		delete m_menu;
		m_menu = 0;
	}
	m_menu = menu;
}

Menu* MenuStateBase::menu()
{
	return m_menu;
}

void MenuStateBase::update(const Uint8* keys)
{
	if(m_menu) {
		m_menu->update(keys);
	}
}
