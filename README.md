# rainbru-soccer

*rainbru-soccer* is a fork of 
[Tux Football](https://tuxfootball.sourceforge.net), 
a 2D football game reminiscent of sensible soccer and Kick Off from days
gone past. 

[![Logo screenshot](doc/screenshot-logo-tn.png)](doc/screenshot-logo.png?raw=true)
[![Logo screenshot](doc/screenshot-tn.png)](doc/screenshot.png?raw=true)

# Controls

The player closest to the ball is the active player; after touch can be
applied by pressing in a particular direction straigt after a kick. The
power of a kick is determined by how long you hold down the kick button
for.

Default Controls for player 1 are :

Key         | Action
------------|------------------
Ctrl		|	Kick
Shift		|	Pass
Arrow Kets	|	Move around

# Dependencies

On debian-based systems :

	sudo apt install cmake libsdl2-mixer-dev libsdl2-image-dev libsdl2-dev gettext blender optipng

On arch-based ones :

	sudo pacman -S blender optipng sdl2_mixer

# Graphics data generation

Before build and installing the game, you'll have to generate data 
(may be long):

	cd data/graphics/
	./create.sh

# Building

The project uses cmake as build system. The game will search datas (graphics and
sounds) in */usr/share* so you'll have to install it before running :

	mkdir build
	cd build/
	cmake ..
	make
	sudo make install         # Needed to install graphic files
	./rainbru-soccer

Have fun!

